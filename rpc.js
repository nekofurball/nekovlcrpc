const DiscordRPC = require("discord-rpc");
var fs = require("fs");
var configjson = fs.readFileSync("config.json");
var config = JSON.parse(configjson);
var VlcService = require("droopy-vlc"),
    vlc = new VlcService("http://:"+config.vlcHTTPpw+"@localhost:8080");
console.log(`Loading NekoVLCRPC v1.0.0`);
function htfix(text) {
  return text
      .replace(/(&#39;)/g,"'")
      .replace(/(&amp;)/g,"&")
      .replace(/(&lt;)/g,"<")
      .replace(/(&gt;)/g,">")
      .replace(/(&quot;)/g,'"')
}
DiscordRPC.register('449127669737455616');
const rpc = new DiscordRPC.Client({ transport: 'ipc' });
let vStt = "Paused"
let vFn = "none"
let vTr = "0:00"
let vDd = "0:00"
let vStat = "Paused"
function vlcUpd() {
  vlc.status().then(function(status) {
    vStt = status.state
    if(status.state === "playing") vStat = "Playing"
    if(status.state === "paused") vStat = "Paused"
   vFn = htfix(status.filename)
   vTr = status.timeDisplay
   vDd = status.durationDisplay
   rpc.setActivity({
    details: vStat,
    state: vFn,
    largeImageKey: "vlc-logo",
    smallImageKey: vStt,
    smallImageText: vTr + " - " + vDd,
    instance: false,
  });
  });
}
rpc.on('ready', () => {
  console.log("Ready to use!")
 vlcUpd()
 setInterval(vlcUpd, 2000)
});


rpc.login(config.clientId).catch(console.error);
